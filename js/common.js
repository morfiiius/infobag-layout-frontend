  function resizeFilterBtn() {

      $(".heat_catalog_bottom .item").each(function() {
          var $thiss = $(this),
              $href = $thiss.attr("href"),
              $sib = $thiss.siblings("a"),
              $thissPar = $thiss.parents(".heat_catalog_btn")
          if ($(window).width() < 768) {
              console.log($sib)
              $sib.appendTo($href);
          } else {
              // $sib.appendTo($thissPar);;
              // console.log($sib)
          }
      });
  }
  $(window).resize(function() {
      resizeFilterBtn();
      if ($(window).width() < 450) {
          $(".section_cards").removeClass("list");
          $(".view__right a:nth-of-type(1)").addClass("active").siblings().removeClass("active");
      }
      if ($(window).width() >= 450 && $(".card_info").length) {
          $(".section_cards").addClass("list");

      }
  })
  $(document).ready(function() {
      $(".filter__item a").click(function(e) {
          e.preventDefault();
          $(this).parent().toggleClass("active").find(".filter__dropdown").slideToggle();

      })
      $(".personal_area_message__dialogs a").click(function() {

              $(".personal_area_message__list, .personal_area_message__buttons").fadeOut(function() {
                  $(".personal_area_message__open_dialog").fadeIn()
              });
          })
          // $(".personal_area_message__dialogs a").click(function(e) {
          //     e.preventDefault();
          //     $(".personal_area_message__buttons").fadeOut();
          //     $(this).closest("ul").fadeOut(function() {
          //         $(".personal_area_message__open_dialog").fadeIn();
          //     })
          // });
          // $(".back_to_dialogs").click(function(e) {
          //     e.preventDefault();

      //     $(".personal_area_message__open_dialog").fadeOut(function() {
      //     $(".personal_area_message__buttons").fadeIn();

      //         $(".personal_area_message__dialogs").find("ul").fadeIn()
      //     });

      // })

      $(".personal_area__right").css("min-height", ($(".personal_area__left").innerHeight() + 45) + "px");


      if ($(window).width() < 450 && $(".card_info").length) {
          $(".section_cards").removeClass("list");

      }
      if ($(window).width() >= 450 && $(".card_info").length) {
          $(".section_cards").addClass("list");

      }
      if ($(window).width() >= 450 && $(".card_info").length) {
          $(".section_cards").addClass("list");

      }
      $('input').tooltip({ 'trigger': 'focus' });
      $(".view__right a:nth-of-type(2)").click(function() {
          $(".section_cards .cards").fadeOut(300, function() {
              $(".section_cards").addClass("list");
              $(this).fadeIn(300);
          });
          $(this).addClass("active").siblings().removeClass("active")
          return false;
      })
      $(".view__right a:nth-of-type(1)").click(function() {
          $(".section_cards  .cards").fadeOut(300, function() {
              $(".section_cards").removeClass("list");
              $(this).fadeIn(300);
          });
          $(this).addClass("active").siblings().removeClass("active")
          return false;
      })
      $(".search__dropdown li").each(function() {
          var $this = $(this);

          var liLength = $this.find(">ul>li").length;
          if ($this.find("ul").length) {
              $this.find(">a").addClass("a_drop")
              $this.addClass("is-dropdown");
              $this.append("<a href='#' class='drop-a'></a>")
              $this.find(".drop-a").append("<span class='li-length'>" + liLength + "</span>")
          }
      });

      $(".drop-a").on("click", function(e) {
          $(this).parent().toggleClass("active");
          $(this).parent().find(">ul").slideToggle();
          e.preventDefault();
      })
      $('[data-toggle="tooltip"]').tooltip();

      $('.search__select .select').select2({
          dropdownCssClass: 'bigdrop',
          dropdownAutoWidth: true,
          minimumResultsForSearch: Infinity
      });
      $('.title_select').select2({
          dropdownCssClass: 'smolldrop',
          minimumResultsForSearch: Infinity
      });
      $('.status_select').select2({
          dropdownCssClass: 'smolldrop',
          minimumResultsForSearch: Infinity
      });
      $('.search__select .select').on('select2:opening', function(evt) {
          if ($(".search__btn_dropdown button").hasClass('open')) {
              $(".search__btn_dropdown button").click();
          }
      });
      //     console.log(e)
      //     // if ($(".search__btn_dropdown button").hasClass('open')) {
      //         $(".search__btn_dropdown button").click();
      //     // }
      // });
      // $(".search__btn_dropdown button").on("click", function() {
      //     $(this).toggleClass("open");
      //     $(this).parent().find(".search__dropdown").toggle();
      // });
      resizeFilterBtn();
      $(".heat_catalog_bottom .item").on('click', function(e) {
          var $this = $(this);
          $thisColor = $this.find(".img").css("background-color");
          $this.parent().find(".show-all").css("background-color", $thisColor);
          var href = $this.attr('href');
          var $divHref = $this.parents(".heat_catalog_bottom").find(href);

          $this.parent().toggleClass("active").siblings().removeClass("active");
          $divHref.find("li span").css("background", $thisColor);
          $divHref.slideToggle().siblings(".filter_tab").slideUp();
          e.preventDefault();
      });
      $(document).click(function(e) {
          var target = e.target;
          var $btn = $(".search__btn_dropdown button");
          if (!$(target).is('.search__btn_dropdown') && $(".search__btn_dropdown button").hasClass("open") && !$(target).parents().is('.search__dropdown')) {
              $btn.removeClass("open");
              $btn.parent().find(".search__dropdown").hide();
          } else if ($(target).is($btn) || $(target).is(".search__btn_dropdown button>*")) {
              $btn.toggleClass("open");
              $btn.parent().find(".search__dropdown").toggle();
          }
      });
      $('.card_info__large_slider').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          fade: true,
          asNavFor: '.card_info__nav_slider'
      });
      $('.card_info__nav_slider').slick({
          slidesToShow: 6,
          slidesToScroll: 1,
          asNavFor: '.card_info__large_slider',
          dots: false,

          focusOnSelect: true,
          responsive: [

              {
                  breakpoint: 480,
                  settings: {
                      slidesToShow: 4,
                      slidesToScroll: 1
                  }
              }
              // You can unslick at a given breakpoint now by adding:
              // settings: "unslick"
              // instead of a settings object
          ]
      });
  });
  $(window).on("resize", scrollHeightMessage);
  $(window).on("load", scrollHeightMessage)

  function scrollHeightMessage() {
      $(".personal_area__right").css("min-height", ($(".personal_area__left").innerHeight() + 45) + "px");
      var scrollContainer = $(".personal_area_message__dialogs");
      if (scrollContainer.length) {
          $(".personal_area_message__dialogs").css("height", scrollContainer.closest(".personal_area__right").height() - scrollContainer.position().top);
      }
  }
